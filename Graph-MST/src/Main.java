import java.awt.*;
import java.util.*;
import java.lang.*;
import java.io.*;
import javax.swing.JFrame;

import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.visualization.VisualizationImageServer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import org.apache.commons.collections15.Transformer;

public class Main {
    public static ArrayList<Integer[]> data;
    public static VisualizationImageServer vs;
    public static Transformer <Integer, Paint> edgeColor;

    public static void makeGUI(int V, int E, ArrayList<Integer[]> data) {
        UndirectedSparseGraph g = new UndirectedSparseGraph();

        Transformer<Integer, Paint> vertexColor = new Transformer<Integer, Paint>() {
            public Paint transform(Integer i) {
                if(data.get(i)[3] == 2) return Color.RED; //2 means current
                else if (data.get(i)[3] == 1) return Color.BLUE; //1 means visited
                return Color.BLACK;
            }
        };
        edgeColor = new Transformer<Integer, Paint>() {
            public Paint transform(Integer i) {
<<<<<<< 064ec168b1605c0dc329bd15fe6fe1d985523d37
                if (data.get(i)[3] == 1) return Color.BLUE;
                return Color.RED;
=======
                if (data.get(i)[3] == 4) return Color.BLUE;
                return Color.BLACK;
>>>>>>> fixed coloring edges
            }
        };

        for (int i = 0; i < V; i++) {
            g.addVertex(i);
        }

        for (int i = 0; i < E; i++) {
            g.addEdge(i, data.get(i)[1], data.get(i)[2]);
        }

        vs = new VisualizationImageServer(
                new CircleLayout(g), new Dimension(500, 500));

        vs.getRenderContext().setEdgeDrawPaintTransformer(edgeColor);
        vs.getRenderContext().setVertexFillPaintTransformer(vertexColor);
        vs.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        //vs.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller());

        JFrame frame = new JFrame("MST Graph");
        frame.getContentPane().add(vs);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) throws IOException {

        data = new ArrayList<>();
        int lineNumber = 0;
        int nodeNumber = 0;
        HashSet nodes = new HashSet(); //this is only to calculate unique values (here number of nodes)
        File file = null;
        Scanner fileName = new Scanner(System.in);

        boolean good = false;
        while(!good) {
            System.out.print("Input filename: ");
            file = new File(fileName.next());
            try {
                Scanner scan = new Scanner(file);
                while (scan.hasNextLine()) {

                    String line = scan.nextLine();
                    StringTokenizer tokens = new StringTokenizer(line, ",");
                    int tempNodeNumber = Integer.parseInt(tokens.nextToken());
                    nodes.add(tempNodeNumber);
                    Integer[] temp = {tempNodeNumber,
                            Integer.parseInt(tokens.nextToken()),
                            Integer.parseInt(tokens.nextToken()),
                            0
                    };
                    data.add(temp);
                    lineNumber++;
                }
                nodeNumber = nodes.size() - 1;
                good = true;
            } catch (FileNotFoundException e) {
                System.out.println("File not found!");
                good = false;
            }
        }

        /*END OF FILE READING */

        int V = nodeNumber;  // Number of vertices in graph
        int E = lineNumber;  // Number of edges in graph
        Graph graph = new Graph(V, E);

        for (int i = 0; i < E; i++) {
            graph.edge[i].weight = data.get(i)[0];
            graph.edge[i].src = data.get(i)[1];
            graph.edge[i].dest = data.get(i)[2];

            System.out.println("Edge "+i+": "+graph.edge[i].src + " " +
                    graph.edge[i].dest + " " +
                    graph.edge[i].weight + " ");
        }

        makeGUI(V,E,data);

        graph.KruskalMST();

        graph.KruskalMST();
        graph.showMatrix();
        graph.dfs();
        graph.printPath();
        graph.clearAll();
        graph.paintPath();
    }
}