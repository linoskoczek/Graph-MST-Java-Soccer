import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class Graph {
    // A class to represent a graph edge
    class Edge implements Comparable<Edge>
    {
        int src, dest, weight;

        // Comparator function used for sorting edges based on
        // their weight
        public int compareTo(Edge compareEdge)
        {
            return this.weight-compareEdge.weight;
        }
    };

    // A class to represent a subset for union-find
    class subset
    {
        int parent, rank;
    };

    int V, E;    // V-> no. of vertices & E->no.of edges
    Edge edge[]; // collection of all edges
    private boolean visited[] = null;
    private int[][] matrix = null;
    private List<Integer> visitedStack = new LinkedList<>();
    private List<Integer> path = new LinkedList<>();

    // Creates a graph with V vertices and E edges
    Graph(int v, int e)
    {
        V = v;
        E = e;
        edge = new Edge[E];
        for (int i=0; i<e; ++i)
            edge[i] = new Edge();
        visited = new boolean[V];
    }

    // A utility function to find set of an element i
    // (uses path compression technique)
    int find(subset subsets[], int i)
    {
        // find root and make root as parent of i (path compression)
        if (subsets[i].parent != i)
            subsets[i].parent = find(subsets, subsets[i].parent);

        return subsets[i].parent;
    }

    // A function that does union of two sets of x and y
    // (uses union by rank)
    void Union(subset subsets[], int x, int y)
    {
        int xroot = find(subsets, x);
        int yroot = find(subsets, y);

        // Attach smaller rank tree under root of high rank tree
        // (Union by Rank)
        if (subsets[xroot].rank < subsets[yroot].rank)
            subsets[xroot].parent = yroot;
        else if (subsets[xroot].rank > subsets[yroot].rank)
            subsets[yroot].parent = xroot;

            // If ranks are same, then make one as root and increment
            // its rank by one
        else
        {
            subsets[yroot].parent = xroot;
            subsets[xroot].rank++;
        }
    }

    // The main function to construct MST using Kruskal's algorithm
    void KruskalMST()
    {
        Edge result[] = new Edge[V];  // Tnis will store the resultant MST
        int e = 0;  // An index variable, used for result[]
        int i = 0;  // An index variable, used for sorted edges
        for (i=0; i<V; ++i)
            result[i] = new Edge();

        // Step 1:  Sort all the edges in non-decreasing order of their
        // weight.  If we are not allowed to change the given graph, we
        // can create a copy of array of edges
        Arrays.sort(edge);

        // Allocate memory for creating V ssubsets
        subset subsets[] = new subset[V];
        for(i=0; i<V; ++i)
            subsets[i]=new subset();

        // Create V subsets with single elements
        for (int v = 0; v < V; ++v)
        {
            subsets[v].parent = v;
            subsets[v].rank = 0;
        }

        i = 0;  // Index used to pick next edge

        // Number of edges to be taken is equal to V-1
        while (e < V - 1)
        {
            // Step 2: Pick the smallest edge. And increment the index
            // for next iteration
            Edge next_edge = new Edge();
            next_edge = edge[i++];

            int x = find(subsets, next_edge.src);
            int y = find(subsets, next_edge.dest);

            // If including this edge does't cause cycle, include it
            // in result and increment the index of result for next edge
            if (x != y)
            {
                result[e++] = next_edge;
                Union(subsets, x, y);
            }
            // Else discard the next_edge
        }

        // print the contents of result[] to display the built MST
        System.out.println("Following are the edges in the constructed MST");
        for (i = 0; i < e; ++i) {
            System.out.println(result[i].src + " -- " + result[i].dest + " == " +
                    result[i].weight);
<<<<<<< 064ec168b1605c0dc329bd15fe6fe1d985523d37
//            for(int a = 0; a < E; a++) {
//                if(Main.data.get(a)[0] == result[i].weight
//                        && Main.data.get(a)[1] == result[i].src
//                        && Main.data.get(a)[2] == result[i].dest) {
//                    Main.data.get(a)[3] = 1;
////                    Main.vs.getRenderContext().setEdgeDrawPaintTransformer(Main.edgeColor);
////                    Main.vs.repaint();
////                    try {
////                        Thread.sleep(1000);
////                    }
////                    catch(InterruptedException e1) {
////                        System.out.println("Closed while sleeping :(");
////                    }
//                    break;
//                }
//            }
=======
            for(int a = 0; a < E; a++) {
                if(Main.data.get(a)[0] == result[i].weight
                        && Main.data.get(a)[1] == result[i].src
                        && Main.data.get(a)[2] == result[i].dest) {
                    Main.data.get(a)[3] = 4;
                    //Main.vs.getRenderContext().setEdgeDrawPaintTransformer(Main.edgeColor);
                    Main.vs.repaint();
                    try {
                        Thread.sleep(1000);
                    }
                    catch(InterruptedException e1) {
                        System.out.println("Closed while sleeping :(");
                    }
                    break;
                }
            }
>>>>>>> fixed coloring edges
        }

        //making matrix
        matrix = new int[V][V];
        for(int d = 0; d < e; d++) {
            matrix[result[d].src][result[d].dest] = result[d].weight;
            matrix[result[d].dest][result[d].src] = result[d].weight;
        }
    }

    public void printPath() {
        if(path != null) {
            System.out.println();
            for(int i = 0; i < path.size(); i++) {
                if(i!=0) System.out.print(" -> ");
                System.out.print(path.get(i));
            }
            System.out.println();
        }
        else {
            System.out.println("There is no path yet...");
        }
    }

    public void paintPath() {
        if(path != null) {
            System.out.println();
            Main.data.get(path.get(0))[3] = 2;
            for (int i = 1; i < path.size(); i++) {
                Main.data.get(path.get(i-1))[3] = 1;
                Main.data.get(path.get(i))[3] = 2;
                //System.out.print(path.get(i)+" => ");
                //Main.vs.getRenderContext().setEdgeDrawPaintTransformer(Main.edgeColor);
                Main.vs.repaint();
                try {
                    Thread.sleep(1000);
                }
                catch(InterruptedException e1) {
                    System.out.println("Interrupted sleeping :(");
                }
            }
        }
        else {
            System.out.println("Cannot color edges without a path...");
        }
    }

    public void showMatrix() {
        for(int s = 0; s < V; s++) {
            for(int d = 0; d < V; d++) {
                System.out.print(matrix[s][d] + " ");
            }
            System.out.println();
        }
    }

    public void dfs() {
        dfs(0);
    }

    public void dfs(int i)
    {
        int j;
        visited[i] = true;
        path.add(i);

        for ( j = 0; j < V; j++ )
        {
            if ( matrix[i][j] > 0 && !visited[j] ) {
                dfs(j);
                path.add(i);
            }
        }
    }

    public void clearAll()
    {
        for (int i = 0; i < visited.length; i++)
            visited[i] = false;
        for(int i = 0; i < E; i++)
            Main.data.get(i)[3] = 0;
    }

}