package ex1;

import java.util.LinkedList;

public class Letters {

    //the storage and the flag
	private LinkedList<Thread> threads;
    private boolean action;

    //creating threads and putting them into the storage. they're even linked :)
    public Letters(String letters) {
    	
        //creating the storage. we can create new storages for every gotten string of letters(i thought it made sense)
        threads = new LinkedList<>();
        
        //creating threads
    	for (char l : letters.toCharArray()) {
    		Runnable thread = () -> {
    			while (action) {
    				System.out.print(l);
    				try {
    					Thread.sleep(1250);
    				} catch (InterruptedException ie) {}
    			}
    		};
    		
            //putting - each char -> into the thread -> into the storage
    		threads.add(new Thread(thread,"Thread " + l));
    	}
    }

    //stop the thread
    public void stopIt() {action = false;}
    
    //start the thread
    public void beginIt() {
        action = true;
        for (Thread thrd : threads) {
            thrd.start();
        }
    }
    //get the storage with the threads
    public LinkedList<Thread> getThreads() {return threads;}
}