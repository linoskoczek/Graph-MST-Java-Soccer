/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex1;


public class Main {

  public static void main(String[] args) throws InterruptedException {
    Letters letters = new Letters("ABCD");
    for (Thread t : letters.getThreads()) System.out.println(t.getName());
    letters.beginIt();

    Thread.sleep(5000);

    letters.stopIt();
    System.out.println("\nProgram finished working");
  }

}
