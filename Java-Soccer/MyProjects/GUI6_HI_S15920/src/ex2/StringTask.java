package ex2;

public class StringTask implements Runnable {

    //some important things
    private volatile TaskState st;
    private Thread thr;
    private String res;
    private String let;
    private int num;

    //set of states
    protected enum TaskState {
        CREATED,
        RUNNING,
        ABORTED,
        READY
    }

    //assignment to those important things after the values get received
    public StringTask (String l, int x) {
        st = TaskState.CREATED;

        res = "";
        let = l;
        num = x;
    }

    //start and stop the thread
    public void start () {
        if (thr == null) {
            thr = new Thread(this, let);
            thr.start();
        }
    }
    public void abort () {
        st = TaskState.ABORTED;
        thr.interrupt();
    }

    //get the state and the result
    public TaskState getState () {return st;}
    public String getResult () {return res;}

    //checker for the task - if done
    public boolean isDone () {
        boolean ready = (st == TaskState.READY);
        boolean aborted = (st == TaskState.ABORTED);
        return (ready || aborted);
    }

    //thread is running this
    @Override
    public void run () {
        st = TaskState.RUNNING;
        int i = 0;
        while (!thr.isInterrupted() && i < num &&st == TaskState.RUNNING) {
            res = res + let;
            i = i + 1;
        }
        if (st != TaskState.ABORTED) st = TaskState.READY;
    }

}

