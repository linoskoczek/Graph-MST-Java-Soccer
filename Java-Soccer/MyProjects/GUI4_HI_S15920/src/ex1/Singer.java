/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex1;

import java.util.concurrent.atomic.AtomicInteger;


public abstract class Singer {

	private String surname;
	private static AtomicInteger number = new AtomicInteger();
	private int id;
	
	
	public Singer(String surname) {
		this.surname = surname;
		id=number.incrementAndGet();
	}
	
	abstract String sing();
	
	public String toString(){
		return "("+id+") "+surname+": "+sing();
	}
	
	static String loudest(Singer a[]) {
		Singer winner = null;
		int min = 0;
		int max;
		//Through objects
		for (int i = 0; i < a.length; i++) {
			//Through characters
			for (int j = 0; j < a[i].sing().length(); j++) {
				max = 0;
				if (Character.isUpperCase(a[i].sing().charAt(j))) {
					max++;
				}
				//Find winner
				if (max > min) {
					winner = a[i];
					min = max;
				}
			}
		}
		//Output winner
		return winner.toString();
	}

	
}
