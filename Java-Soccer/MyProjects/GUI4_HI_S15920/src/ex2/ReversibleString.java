package ex2;

public class ReversibleString implements Reversible {

	private String s;
	private StringBuilder sb;
	
	public ReversibleString(String string) {
		s = string;
	}

	@Override
	public Reversible reverse() {
		sb = new StringBuilder();
		sb.append(s).reverse();
		s = sb.toString();
		return this;
	}
	
	public String toString(){
		return s;
	}
	
	public void setText(){
		StringBuilder sb2 = new StringBuilder();
		sb2.append("Text ").reverse();
		s = sb.append(sb2).toString();
	}

}
