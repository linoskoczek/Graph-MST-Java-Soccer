package ex2;

public class ReversibleDouble implements Reversible {

	private double i;
	private String str;
	
	public ReversibleDouble(int i) {
		this.i = i;
	}

	@Override
	public Reversible reverse() {
		i = 1/i;
		return this;
	}

	public void setValue() {
		this.i += 10;
	}

	public String toString(){
		str = String.valueOf(i);
		return str;
	}
}
