/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex1;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.DoubleSummaryStatistics;
import java.util.HashMap;

public class Calc {
    String output = "";

    public double add(double a, double b) {
        String s = a+" + "+b;
        return doCalc(s);
    }

    public double substract(double a, double b) {
        String s = a+" - "+b;
        return doCalc(s);
    }

    public double multiply(double a, double b) {
        String s = a+" * "+b;
        return doCalc(s);
    }

    public double divide(double a, double b) {
        String s = a+" / "+b;
        return doCalc(s);
    }

    abstract class Node {
        abstract BigDecimal evaluate();
    }
    //	         class ValueNode extends Node{
//	            BigDecimal value;
//		        BigDecimal evaluate(){
//                    return value;
//                }
//	         }
    //-----------------------------------------------------------------------------
    abstract class OperationNode extends Node{
        Node left;
        Node right;
        abstract BigDecimal evaluate();
    }

    class AdditionNode extends OperationNode{
        AdditionNode(final BigDecimal x, final BigDecimal y){
            left = new Node() {
                @Override
                BigDecimal evaluate() {
                    return x;
                }
            };
            right = new Node() {
                @Override
                BigDecimal evaluate() {
                    return y;
                }
            };
        }
        public BigDecimal evaluate() {
            return left.evaluate().add(right.evaluate()).setScale(10, RoundingMode.HALF_UP).stripTrailingZeros();
        }
    }
    class SubtractionNode extends OperationNode{
        SubtractionNode( BigDecimal x, final BigDecimal y){
            left = new Node() {
                @Override
                BigDecimal evaluate() {
                    return x;
                }
            };
            right = new Node(){
                @Override
                BigDecimal evaluate() {
                    return y;
                }
            };
        }
        BigDecimal evaluate() {
            return left.evaluate().subtract(right.evaluate()).setScale(10, RoundingMode.HALF_UP).stripTrailingZeros();
        }
    }
    class MultiplicationNode extends OperationNode{
        MultiplicationNode(final BigDecimal x, final BigDecimal y){
            left = new Node() {
                @Override
                BigDecimal evaluate() {
                    return x;
                }
            };
            right = new Node(){
                @Override
                BigDecimal evaluate() {
                    return y;
                }
            };
        }
        BigDecimal evaluate() {
            return left.evaluate().multiply(right.evaluate()).setScale(10, RoundingMode.HALF_UP).stripTrailingZeros();
        }
    }
    class DivisionNode extends OperationNode{
        DivisionNode(final BigDecimal x, final BigDecimal y){
            left = new Node() {
                @Override
                BigDecimal evaluate() {
                    return x;
                }
            };
            right = new Node(){
                @Override
                BigDecimal evaluate() {
                    return y;
                }
            };
        }
        BigDecimal evaluate() {
            return left.evaluate().divide(right.evaluate(), 10, RoundingMode.HALF_UP).stripTrailingZeros();
        }
    }
    //-----------------------------------------------------------------------------
    public Double doCalc(String string){
        try {
            String[] tokens = string.split("\\s+");
            BigDecimal a = new BigDecimal(tokens[0]);
            BigDecimal b = new BigDecimal(tokens[2]);
            String op = tokens[1];

            HashMap<String, OperationNode> m = new HashMap<>();
            m.put("+", new AdditionNode(a, b));
            m.put("-", new SubtractionNode(a, b));
            m.put("*", new MultiplicationNode(a, b));
            m.put("/", new DivisionNode(a, b));

            output = string+" = "+m.get(op).evaluate();
            return m.get(op).evaluate().doubleValue();
        }catch (Exception s){
            output = string+" = ERR";
            return Double.NaN;
        }
    }

    public String toString(){
        return output;
    }
}  
