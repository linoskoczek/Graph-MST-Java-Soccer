/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex1;


public class Main {
  public static void main(String[] args) {

    Calc c = new Calc();
    double l1 = c.add(1, 3);
    System.out.println(c);
    double l2 = c.substract(1.11, 5.0);
    System.out.println(c);
    double l3 = c.multiply(3, 1.9);
    System.out.println(c);
    c.divide(l1, l2);
    System.out.println(c);
    double x = c.divide(l1, 0.0);

    System.out.println(c);
    System.out.println(x);
    System.out.println(l3);

    JFrame jFrame = new JFrame("Calculator");
    JLabel res = new JLabel();
    JLabel op = new JLabel();
    JButton[] numbers = new JButton[10];

    int e = 0;
    numbers[0] = new JButton(Integer.toString(0));
    numbers[0].setBounds(60, 270, 50, 50);
    jFrame.add(numbers[0]);

    for (int i = 1; i <= 3; i++) {
      numbers[i] = new JButton(Integer.toString(i));
      numbers[i].setBounds(10+e, 220, 50, 50);
      jFrame.add(numbers[i]);
      e+=50;
    }
    e=0;
    for (int i = 4; i <= 6; i++) {
      numbers[i] = new JButton(Integer.toString(i));
      numbers[i].setBounds(10+e, 170, 50, 50);
      jFrame.add(numbers[i]);
      e+=50;
    }
    e=0;
    for (int i = 7; i <= 9; i++) {
      numbers[i] = new JButton(Integer.toString(i));
      numbers[i].setBounds(10+e, 120, 50, 50);
      jFrame.add(numbers[i]);
      e+=50;
    }
    JButton plus = new JButton("+");
            plus.setBounds(160, 120, 50, 100);
            operations.put(plus.getText(), plus);
            jFrame.add(plus);
    JButton minus = new JButton("-");
            minus.setBounds(160,70,50,50);
            operations.put(minus.getText(), minus);
            jFrame.add(minus);
    JButton multiply = new JButton("*");
            multiply.setBounds(110,70,50,50);
            operations.put(multiply.getText(), multiply);
            jFrame.add(multiply);
    JButton divide = new JButton("/");
            divide.setBounds(60,70,50,50);
            operations.put(divide.getText(), divide);
            jFrame.add(divide);
    JButton erase = new JButton("AC");
            erase.setBounds(10,70,50,50);
            jFrame.add(erase);
    JButton equal = new JButton("=");
            equal.setBounds(160,220,50,100);
            jFrame.add(equal);
//    JButton point = new JButton(".");
//            point.setBounds(110,270,50,50);
//            jFrame.add(point);
    JButton delete = new JButton("←");
            delete.setBounds(10,270,50,50);
            jFrame.add(delete);


    res.setFont(new Font(Font.SERIF, Font.BOLD, 15));
    res.setBounds(15,15,200,50);
    res.setPreferredSize(new Dimension(160, 20));
    res.setEnabled(true);
    res.setBackground(Color.WHITE);
    res.setForeground(Color.BLACK);
    res.setHorizontalAlignment(JTextField.RIGHT);
    res.setText("0.0");

    op.setFont(new Font(Font.SERIF, Font.PLAIN, 12));
    op.setBounds(0,0,200,50);
    op.setPreferredSize(new Dimension(160, 20));
    op.setEnabled(true);
    op.setBackground(Color.WHITE);
    op.setForeground(Color.GRAY);
    op.setHorizontalAlignment(JTextField.RIGHT);
    op.setText("");


    //input numbers
    for (int i = 0; i < 10; i++){
      final String num = Integer.toString(i);
      numbers[i].addActionListener((ActionEvent ae) -> {
          if(oper.equals("=")){
            op.setText(num);
            oper ="";
          }
          else if(op.getText().equals("0"))
            op.setText(num);
          else
            op.setText(op.getText()+num);
      });
    }

    //input operations
    for (HashMap.Entry<String, JButton> entry : operations.entrySet()) {
      entry.getValue().addActionListener((ActionEvent ae) -> {
          if (oper.equals("=")){
            oper = entry.getKey();
            pre = res.getText();
          }
          else if (res.getText() == "0.0" && op.getText() == ""){
            pre = res.getText();
          }
          else{
            oper = entry.getKey();
            pre = op.getText();
          }
          op.setText(pre + " " + entry.getKey() + " ");

      });
    }

    //other operations
    delete.addActionListener((ActionEvent ae) -> {
        oper ="del";
        String str=op.getText();
        int p = str.length();
        if(p-2!=0 && p-2 > 0)
          op.setText(op.getText().substring(0,p-2));
        else
          op.setText("0");
    });

    erase.addActionListener((ActionEvent ae) -> {
        oper ="";  //AC
        pre=""; //AC
        op.setText("");
        res.setText("");
    });

//    point.addActionListener(new ActionListener(){
//      public void actionPerformed (ActionEvent ae){
//        if(oper.equals("=")){
//          op.setText("0.");
//          oper=".";
//        }
//        else
//          op.setText(op.getText()+".");
//      }
//    });

    equal.addActionListener((ActionEvent ae) -> {
      if (c.doCalc(op.getText()).toString() == "NaN") {
        op.setText(op.getText() + " = ");
        res.setText(c.doCalc(op.getText()).toString() + ", clear field!");
        oper = "=";
      }
      else
      {
        op.setText(op.getText() + " = ");
        res.setText(c.doCalc(op.getText()).toString());
        oper = "=";
      }
    });


    //JFrame properties
    jFrame.add(op);
    jFrame.add(res);

    jFrame.setBounds(300, 200, 220, 350);
    jFrame.setLayout(null);
    jFrame.pack();
    jFrame.setSize(220, 350);
    jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jFrame.setVisible(true);

  }
}
