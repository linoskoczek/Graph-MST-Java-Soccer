/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class CountWords {

	public String fpath;
	public String regex = "\\W+";
	public Scanner scanner;
	public Scanner delimiter;
	public Map<String, Integer> hashMap;
	public List<String> result;

	public CountWords(String fname) {
		fpath = fname;
	}

	public List<String> getResult() throws IOException {

		//Reading file's data into a HashMap
		hashMap = new LinkedHashMap<String, Integer>();
		scanner = new Scanner(new File(fpath));
		delimiter = scanner.useDelimiter(regex);
		while (delimiter.hasNext()) {
			String value = delimiter.next();
			Integer key = hashMap.get(value);
			if (key != null) {
				hashMap.put(value, key + 1);
			} else {
				hashMap.put(value, 1);
			}
		}
		delimiter.close();

		//Converting a HashMap's entries into Strings and putting them in a List
		result = new ArrayList<>();
		for (Map.Entry<String, Integer> entry : hashMap.entrySet()) {
			String key = entry.getKey().toString();
			Integer value = entry.getValue();
			String union = key + " " + value;
			result.add(union);
		}

		return result;
	}

}
