/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {

	private static Scanner scan;
	public static ArrayList<Integer> list;

	public static void main(String[] args) throws FileNotFoundException {
		String fname = System.getProperty("user.home") + "/tab.txt";

		try {
			
			scan = new Scanner(new File(fname));
			list = new ArrayList<Integer>();
			
			//Filling the AL with values
			while (scan.hasNext()){
				list.add(scan.nextInt());
			}
			
			//Finding max value
			for(int i=0; i<list.size(); i++)
				System.out.print(list.get(i)+" ");
			System.out.println();
			int max = Collections.max(list);
			System.out.println(max);
			
			//Finding indexes
			int[] indexes = new int[list.size()];
			for(int i=0; i<list.size(); i++){
				if(list.get(i) == max){
					indexes[i] = i;
					System.out.print(indexes[i]+" ");
				}
			}
			
		} catch (Exception e) {
			System.out.println("***");
		}

	}
}
