/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Finder {
	
	//File's path is stored here
	String filespath;

	//File's path gets assigned here
	public Finder(String fname) {
		filespath = fname;
	}
	
	//Reading the file
	public BufferedReader read() throws FileNotFoundException{
		return new BufferedReader(new FileReader(filespath));		
	}

	
	
	//Well well well, how many ifs do we have here?
	public int getIfCount() throws IOException {
		return this.getStringCount("if");	
	}

	//Let's count variables!
	public int getStringCount(String string) throws IOException {
		
		//Method's counter
		int counter = 0;
		
		//Buffering characters from the file
		BufferedReader fl = read();
		//Defining a variable for the line
		String line;
		
		//Counting itself
		while ((line = fl.readLine()) != null) {
			
			//Splitting each line of the file into separate words
			String[] words = line.split("\\s*[^a-zA-Z0-9]+\\s*");
			//System.out.println(line);
			//Line's variable
			int word = 0;
			
			//Comparing given word with each line's word
			while(word < words.length){
				 Pattern p = Pattern.compile(string);
		         Matcher m = p.matcher(words[word]);
				 while(m.find())counter++;
				 word++;
			}
		}
		
		//Returning the counted value
		return counter;
	}

}    
