package ex1;

public class BankCustomer {

	
	//The person and an object to the person'saccount
	public Person person;
	public Account account = new Account();
	
	//A person
	public BankCustomer(Person person) {
		this.person = person;
	}

	//The person's account
	public Account getAccount() {
		return this.account;
	}
	
	//The balance with the interest will be displayed on the console
	@Override
	public String toString() {
		return "Client: "+ this.person.name +" account balance "+ this.account.balance; 	
	} 

}
