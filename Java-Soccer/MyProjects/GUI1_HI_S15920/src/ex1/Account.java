package ex1;


public class Account {

	//The balance and the interest(for each customer is the same)
	public double balance;
	public static double interest;
	
	//Setting the interest rate
	public static void setInterestRate(double d) {
		if(d>=0)
			interest = d;
	}

	//Getting richer
	public void deposit(int i) {
		if (i > 0)
			this.balance += i;
	}

	//Paying off debts :)
	public void transfer(Account account, int i) {
		if (i > 0 && balance>=i){
			this.withdraw(i);
			account.deposit(i);
		}
	}

	//Getting poorer
	public void withdraw(int i) {
		if (i > 0 && balance>=i)
			this.balance -= i;
	}

	//Some extra money from the air
	public void addInterest() {
		this.balance += (balance*interest/100);
	}
	
}
