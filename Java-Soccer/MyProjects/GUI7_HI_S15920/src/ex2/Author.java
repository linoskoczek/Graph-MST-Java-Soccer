/**
 *
 *  @author Halych Illia S15920
 *
 */
package ex2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Author extends Thread {
    //-----------------------------------//FIELDS
    public BlockingQueue<String> q = new ArrayBlockingQueue<>(1000);
    private boolean flag = true;
    String[] args;
    //-----------------------------------//CONSTRUCTOR
    public Author(String[] args){
        this.args = args;
    }
    //-----------------------------------//RUN STUFF
    public void run() {
        //-----------------------------------//LOOP
        for(int i = 0; i < args.length; i++){
            //-----------------------------------//PUT&SLEEP
            try {
                Thread.sleep(1000);
                q.put(args[i]);
            } catch (InterruptedException e) {}
            //-----------------------------------//TURN OFF
        }setFlag(false);
        //----------------------------------
    }
    //-----------------------------------//GET FLAG
    public boolean getFlag()
        {return flag;}
    //-----------------------------------//SET FLAG
    public void setFlag(boolean control)
        {flag = control;}
    //-----------------------------------//FINALLY I FINISHED IT -_-
}
