/**
 *
 *  @author Halych Illia S15920
 *
 */
package ex2;

public class Writer extends Thread {
    //-----------------------------------//GET AUTHOR'S TEXT
    Author text;
    //-----------------------------------//CONSTRUCTOR
    public Writer(Author text){
        this.text = text;
    }
    //-----------------------------------//RUN
    @Override
    public void run() {
        //-----------------------------------//GET FROM QUEUE
        while(text.getFlag()){
            //-----------------------------------//TRY TO GET
            try {
                System.out.println(text.q.take());
            } catch (InterruptedException ie) {}
            //-----------------------------------//YEAH
        }
        //-----------------------------------//GOOD
    }
    //-----------------------------------//JOB
}
