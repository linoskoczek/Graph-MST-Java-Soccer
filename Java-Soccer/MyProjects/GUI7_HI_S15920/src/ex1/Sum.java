package ex1;

import java.util.ArrayList;

public class Sum extends Thread {
	//-----------------------------------------------------
	private Storage storage;
	private ArrayList<Integer> locPack;
	private int sum = 0;
	private boolean done = false;
	//-----------------------------------------------------
	public Sum(Storage pack) {
		this.storage = pack;
	}
	//-----------------------------------------------------
	public void run() {
		try {
			while (true) {
				locPack = new ArrayList<>();
				locPack = storage.getPack();
				int counter = 0;
				// ------------------------------------
				while (counter < 200) {
					if(storage.getNumW() < storage.getNum()) {
						sum += locPack.get(counter);
						counter++;
						// -------------------------------------
						storage.setNumW(storage.getNumW() + 1);
						// -------------------------------------
						if((counter % 100) == 0){
							System.out.println("counted the weight of "+storage.getNumW()+" goods");
						}
					}
					else {
						System.out.println(sum);
						done = true;
						break;
					}
				}
				// ---------------------------------
				if (done) break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//-----------------------------------------------------
}
