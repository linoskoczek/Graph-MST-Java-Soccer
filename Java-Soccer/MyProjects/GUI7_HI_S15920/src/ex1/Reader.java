package ex1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Reader extends Thread {
//-----------------------------------------------------
	private Storage storage;
	private ArrayList<Integer> pack;
	private boolean done = false;
//-----------------------------------------------------
	public Reader(Storage storage) {
		this.storage = storage;
	}
//-----------------------------------------------------
	public void run() {
		// Reading from file
		String sCurrentLine;
		FileReader in = null;
		try {
			in = new FileReader("../Goods.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(in);
		// packing to array
		// ---------------------------------------------
		try {
			while (!Reader.interrupted()) {
				//---------------------------------
				int counter = 0;
				pack = new ArrayList<>();
				boolean ready = false;
				//------------------------------------
				while (counter < 200) {
					if ((sCurrentLine = br.readLine()) != null) {
						counter++;
						// -------------------------------------
						String[] line = sCurrentLine.split(" ");
						int weight = Integer.parseInt(line[1]);
						storage.setNum(storage.getNum() + 1);
						pack.add(weight);
						if (counter == 199) ready = true;
					}else {
						done = true;
						break;
					}
				}
				//---------------------------------
					if((counter % 200) == 0 && ready)
						if (storage.getNum() != storage.getNumW())
							System.out.println("created " + storage.getNum() + " objects");
					storage.setPack(pack);
				//---------------------------------
				if (done) this.interrupt();
			}
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		// ----------------------------------------------
	}

}
