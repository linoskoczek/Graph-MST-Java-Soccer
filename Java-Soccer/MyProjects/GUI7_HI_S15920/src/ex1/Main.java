/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex1;

import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
			
		Storage storage = new Storage();
		
		Thread a = new Reader(storage);
		Thread b = new Sum(storage);
		
		a.start();
		b.start();
	}
}
