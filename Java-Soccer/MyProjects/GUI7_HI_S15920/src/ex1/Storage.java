package ex1;

import java.util.ArrayList;

public class Storage {
	//-------------------------------------------------------------
	private ArrayList<Integer> wg = new ArrayList<>();
	private ArrayList<Integer> send;
	private boolean packed = false;
	private int counter = 0;
	private int counterW = 0;
	private int start = 0;
	//-------------------------------------------------------------


	// invoked by A to set a new text
	synchronized public void setPack(ArrayList<Integer> pack) {

		while (packed) {
			try
				{wait();}
			catch (InterruptedException exc)
				{}
		}
		//-----------------------------------------
		for(int x : pack) wg.add(x);
		//-----------------------------------------
		packed = true;
		notify();
	}
	//-------------------------------------------------------------


	// invoked by B to get a text
	synchronized public ArrayList<Integer> getPack() {
		while (!packed) {
			try
				{wait();}
			catch (InterruptedException exc)
				{}
		}
		//-----------------------------------------
		send = new ArrayList<>();
		for(int i = start; i < wg.size(); i++){
			send.add(wg.get(start));
		}
		start = wg.size();
		//-----------------------------------------
		packed = false;
		notify();
		return send;
	}
//-------------------------------------------------------------
	public int getNum(){
		return counter;
	}
	public void setNum(int x){
		counter = x;
	}

	public int getNumW(){
		return counterW;
	}
	public void setNumW(int x){
		counterW = x;
	}
}