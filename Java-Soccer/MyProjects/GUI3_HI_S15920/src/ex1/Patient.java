package ex1;

public abstract class Patient {

	public String name;
	
	public Patient(String pname) {
		name = pname;
	}
	
	public String surname(){
		return name;
	}
	
	abstract String disease();
	
	abstract String treatment();
}
