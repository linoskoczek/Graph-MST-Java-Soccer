package ex1;

public class SickForDyspepsia extends Patient {
	
	public SickForDyspepsia(String pname) {
		super(pname);
	}

	@Override
	String disease() {
		return "dyspepsia";
		
	}
	
	@Override
	String treatment() {
		return "activated charcoal";
	}
}
