package ex1;

public class SickForLeg extends Patient {

	public SickForLeg(String pname) {
		super(pname);
	}

	@Override
	String disease() {
		return "leg";
		
	}
	
	@Override
	String treatment() {
		return "plaster cast";
	}

}
