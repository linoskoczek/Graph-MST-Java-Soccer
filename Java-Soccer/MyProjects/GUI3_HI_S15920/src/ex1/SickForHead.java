package ex1;

public class SickForHead extends Patient {

	public SickForHead(String pname) {
		super(pname);
	}

	@Override
	String disease() {
		return "head";
		
	}
	
	@Override
	String treatment() {
		return "aspirin";
	}

}
