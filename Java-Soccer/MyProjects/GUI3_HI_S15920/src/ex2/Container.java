package ex2;

import java.util.ArrayList;

public abstract class Container {

	public PriceList priceList = PriceList.getInstance();
	public ArrayList<Flower> container = new ArrayList<>();	
	StringBuilder sb = null;
	
	public Container(){}
	
	@Override
	public String toString() {
		String string = "";
		for (int i = 0; i < container.size(); i++) {
			string += container.get(i).name() + " ";
		}
		return string;
	}
		
	public void addEl(Flower f)
		{ container.add(f); }
	
	public ArrayList<Flower> getList()
		{ return container; }
	
}
