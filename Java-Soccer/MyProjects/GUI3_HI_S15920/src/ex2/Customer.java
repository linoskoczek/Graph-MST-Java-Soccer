package ex2;

import java.util.Iterator;

public class Customer {

	public String name;
	public double balance;
	public ShoppingCart sc;
	public PriceList pl = PriceList.getInstance();
	public Flower flower = null;
	
	public Customer(String string, double i) {
		name = string;
		balance = i;
		sc = new ShoppingCart(); //NEW CART FOR EACH CUSTOMER
	}

	//TAKES A CART
	public ShoppingCart getShoppingCart() 
		{ return this.sc; }

	//ADDS TO THE CART
	public void get(Flower flower) 
		{ sc.addEl(flower); }
	
	//PAYS
	public void pay() {
		Flower flower = null;
		for (Iterator<Flower> iterator = sc.getList().iterator(); iterator.hasNext();) {
			flower = iterator.next();
			if (pl.ifIn(flower.name()) && balance >= pl.get(flower.name()) * flower.amount())
				balance -= pl.get(flower.name()) * flower.amount();
			else
				iterator.remove();
		}
	}

	//BALANCE REMAINING
	public double getCash() 
		{ return balance; }
	
	//PACKS
	public void pack(Box johnBox) {
		for (Iterator<Flower> iterator = sc.getList().iterator(); iterator.hasNext();) {
			flower = iterator.next();
			if(flower != null)
				johnBox.addEl(flower);
			iterator.remove();
		}
	}
}
