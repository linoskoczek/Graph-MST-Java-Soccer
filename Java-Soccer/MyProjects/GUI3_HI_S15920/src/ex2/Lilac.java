package ex2;

public class Lilac extends Flower{

	public Lilac(int amount) {
		super(amount);
	}

	@Override
	public String color(){
		return "violet";
	}
	
	@Override
	public String name(){
		return "lilac";
	}
}
