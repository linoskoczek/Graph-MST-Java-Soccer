package ex2;

import java.util.ArrayList;

public class Box extends Container {

	public Customer customer;
	
	public Box(Customer customer) 
		{ this.customer = customer; }

	public ArrayList<Flower> getList() 
		{ return container; }
	
	@Override
	public String toString() {
		sb = new StringBuilder();
		for (int i = 0; i < container.size(); i++) {
			sb.append(container.get(i).name() + ", which is " + container.get(i).color() + ", times "
					+ container.get(i).amount()+"\n");
		}
		return sb.toString();
	}
}
