package ex2;

public class ShoppingCart extends Container {

	public ShoppingCart() {}

	public Flower get(int f) 
		{ return container.get(f); }

	public void removeItem(int i)
		{ container.remove(i); }
	
	@Override
	public String toString() {
		sb = new StringBuilder();
		for (Flower x : container) {
			sb.append("\n"+x.name()+", which is "+x.color()+", "+x.amount()+" times");
		}sb.append("\n");
		return sb.toString();
	}

}
