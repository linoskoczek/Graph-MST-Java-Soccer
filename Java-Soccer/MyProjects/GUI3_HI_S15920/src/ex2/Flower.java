package ex2;

public abstract class Flower {

	public int amount;
	
	public Flower(int a){
		amount = a;
	}

	//AMOUNT
	public int amount()
		{ return amount; }
	
	//NAME
	public abstract String name();
	
	//COLOR
	public abstract String color();
	
}
