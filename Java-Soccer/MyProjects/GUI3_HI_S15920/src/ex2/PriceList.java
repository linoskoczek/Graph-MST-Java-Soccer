package ex2;

import java.util.HashMap;
import java.util.Map;

public class PriceList {

	public Map<String, Double> map = new HashMap<String, Double>();

    private static PriceList instance = null;

    private PriceList() {}

    public static PriceList getInstance() {
        if (instance == null)
            instance = new PriceList();
        return instance;
    }

    //PUTS THE PRICE
    public void put(String string, Double d) 
    	{ map.put(string,d); }
	
    //GETS THE PRICE
    public Double get(String name)
    	{ return map.get(name); }
    
    //CHECKS PRICES
    public boolean ifIn(String s)
    	{ return map.containsKey(s); }
  
}
