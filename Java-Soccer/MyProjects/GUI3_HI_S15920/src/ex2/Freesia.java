package ex2;

public class Freesia extends Flower {

	public Freesia(int amount) {
		super(amount);
	}

	@Override
	public String color(){
		return "yellow";
	}

	@Override
	public String name(){
		return "freesia";
	}
}
