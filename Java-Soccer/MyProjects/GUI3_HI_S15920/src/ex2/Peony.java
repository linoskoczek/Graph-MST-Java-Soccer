package ex2;

public class Peony extends Flower{

	public Peony(int amount) {
		super(amount);
	}

	@Override
	public String color(){
		return "pink";
	}
	
	@Override
	public String name(){
		return "peony";
	}
}
