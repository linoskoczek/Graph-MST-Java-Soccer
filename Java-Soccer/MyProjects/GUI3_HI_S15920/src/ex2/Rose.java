package ex2;

public class Rose extends Flower{
	
	public Rose(int amount) {
		super(amount);
	}
	
	@Override
	public String color(){
		return "red";
	}
	
	@Override
	public String name(){
		return "rose";
	}
}
