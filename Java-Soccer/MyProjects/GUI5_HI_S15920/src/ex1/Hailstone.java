package ex1;

import java.util.Iterator;

public class Hailstone implements Iterable<Integer> {
	
	private Integer ini;
	public Hailstone(int ini){
		this.ini=ini;
	}
	
	//The main part w/ the anonymous class
	@Override
	public Iterator<Integer> iterator() {
		Iterator<Integer> it = new Iterator<Integer>(){
			
			private Integer num = Hailstone.this.getIni();
			private Boolean one = false;
			@Override
			public boolean hasNext() {return !one;}

			@Override
			public Integer next() {
				if (hasNext()) {
					 if(num == 1){
					 		one = true;
					 }
					int current = num;
					if (!one) {
						num = Hailstone.this.nextCollatzNumber(num);
					}
					return current;
				}
				return null;
			}
		};
		return it;
	}
	
	
	protected Integer getIni(){
		return this.ini;
	}
	public Integer nextCollatzNumber(Integer a){
		//Next element if null
		if(a == null)
			return null;
		//Formula
		return (a%2 == 0) ? (a/2) : (3*a+1);
	}
}
