/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex2;


public class Main {

  public static void main(String[] args) {
    Calc c = new Calc();
    String result = c.doCalc(args[0]);
    System.out.println(result);
  }

}
