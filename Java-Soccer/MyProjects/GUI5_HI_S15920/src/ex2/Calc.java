/**
 *
 *  @author Halych Illia S15920
 *
 */
//beyond compare

package ex2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

public class Calc {
	//-----------------------------------------------------------------------------
	abstract class Node {
        abstract BigDecimal evaluate();
    }
//	         class ValueNode extends Node{
//	            BigDecimal value;
//		        BigDecimal evaluate(){
//                    return value;
//                }
//	         }
	//-----------------------------------------------------------------------------
	abstract class OperationNode extends Node{
        Node left;
        Node right;
		abstract BigDecimal evaluate();
	}

             class AdditionNode extends OperationNode{
                 AdditionNode(final BigDecimal x, final BigDecimal y){
                     left = new Node() {
                         @Override
                         BigDecimal evaluate() {
                             return x;
                         }
                     };
                     right = new Node() {
                         @Override
                         BigDecimal evaluate() {
                             return y;
                         }
                     };
                 }
                 public BigDecimal evaluate() {
                     return left.evaluate().add(right.evaluate());
                 }
             }
	         class SubtractionNode extends OperationNode{
                     SubtractionNode(final BigDecimal x, final BigDecimal y){
                         left = new Node() {
                             @Override
                             BigDecimal evaluate() {
                                 return x;
                             }
                         };
                         right = new Node(){
                             @Override
                             BigDecimal evaluate() {
                                 return y;
                             }
                         };
                     }
	                 BigDecimal evaluate() {
                     return left.evaluate().subtract(right.evaluate());
                 }
             }
	         class MultiplicationNode extends OperationNode{
                    MultiplicationNode(final BigDecimal x, final BigDecimal y){
                        left = new Node() {
                            @Override
                            BigDecimal evaluate() {
                                return x;
                            }
                        };
                        right = new Node(){
                            @Override
                            BigDecimal evaluate() {
                                return y;
                            }
                        };
                    }
	                BigDecimal evaluate() {
                    return left.evaluate().multiply(right.evaluate());
                }
            }
	         class DivisionNode extends OperationNode{
                 DivisionNode(final BigDecimal x, final BigDecimal y){
                     left = new Node() {
                         @Override
                         BigDecimal evaluate() {
                             return x;
                         }
                     };
                     right = new Node(){
                         @Override
                         BigDecimal evaluate() {
                             return y;
                         }
                     };
                 }
                 BigDecimal evaluate() {
                    return left.evaluate().divide(right.evaluate(), 10, RoundingMode.HALF_UP).stripTrailingZeros();
                }
            }
	//-----------------------------------------------------------------------------
                    public String doCalc(String string){
                        try {
                            String[] tokens = string.split("\\s+");
                            BigDecimal a = new BigDecimal(tokens[0]);
                            BigDecimal b = new BigDecimal(tokens[2]);
                            String op = tokens[1];

                            HashMap<String, OperationNode> m = new HashMap<>();
                            m.put("+", new AdditionNode(a, b));
                            m.put("-", new SubtractionNode(a, b));
                            m.put("*", new MultiplicationNode(a, b));
                            m.put("/", new DivisionNode(a, b));

                            return m.get(op).evaluate().toString();
                        }catch (Exception s){
                            return "Invalid command to calc";
                        }
                    }
	//-----------------------------------------------------------------------------
}  
