/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex1;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main extends JFrame implements Runnable {

  public Color[] colors = {Color.RED, Color.GREEN, Color.BLUE, Color.CYAN, Color.ORANGE};
  public int index = 0;

  public static void main(String[] args) {
    SwingUtilities.invokeLater(new Main());

  }

  @Override
  public void run() {
    JButton jButton = new JButton("Click me, cliiiick");

    ActionListener actionListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
          if (source instanceof Component) {
            ((Component) source).setBackground(colors[(index++)%(colors.length)]);
        }
      }
    };

    jButton.addActionListener(actionListener);


    add(jButton, BorderLayout.CENTER);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setPreferredSize(new Dimension(300, 300));
    setSize(300, 300);
    setVisible(true);
    pack();

  }
}
