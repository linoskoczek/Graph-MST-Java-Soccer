package ex2;

import javax.swing.*;
import java.awt.*;

/**
 * Created by tiger on 12.05.17.
 */
public class MyOvalIcon implements Icon {

    private int width = 8;
    private int height = 8;
    private Color color;

    public MyOvalIcon(Color color){
        this.color  = color;
    }

    @Override
    public void paintIcon(Component component, Graphics g, int x, int y) {
        Color temp = g.getColor();
        g.setColor(color);
        g.fillOval(x, y, getIconWidth(), getIconHeight());
        g.setColor(temp);
    }

    @Override
    public int getIconWidth() {
        return width;
    }

    @Override
    public int getIconHeight() {
        return height;
    }


}
