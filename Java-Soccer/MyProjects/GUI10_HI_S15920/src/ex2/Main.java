/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex2;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Main extends JFrame {

  //Main stuff
  private JTextArea area;
  private JScrollPane jScrollPane;
  //Bar
  private JMenuBar jMenuBar;
  private JMenu file, edit, options;
  private JMenu foreground, background, fontSize;
  //FileChooser
  private JFileChooser dialog = new JFileChooser(System.getProperty("user.dir"));
  private String currentFile = "Untitled";
  private boolean changed = false;
  //Colors
  private HashMap<String, Color> colors = new HashMap<>();
  private ArrayList<JMenuItem> itemsFG = new ArrayList<>();
  private ArrayList<JMenuItem> itemsBG = new ArrayList<>();
  //EDIT
  private HashMap<String, String> edits = new HashMap<>();
  private HashMap<Integer, KeyStroke> shortCutsEDIT = new HashMap<>();
  private ArrayList<JMenuItem> itemsEDIT = new ArrayList<>();
  //Font size
  private ArrayList<JMenuItem> itemFONT = new ArrayList<>();
  private ArrayList<Integer> fontSizes = new ArrayList<>();
  //Index
  private int e = 0;
  //Constractor
  public Main(){
    setTitle("Simple editor - "+currentFile);
    //
    area = new JTextArea();
    area.setFont(new Font("Calibri", Font.BOLD, 12));
    jScrollPane = new JScrollPane(area, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    add(jScrollPane, BorderLayout.CENTER);

    //First level
    jMenuBar = new JMenuBar();
    setJMenuBar(jMenuBar);
    createMenuBar();

    //Second level
    createFile();
    createEdit();
    createOptions();

    //
    pack();
    area.addKeyListener(k1);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(700, 300);
    setVisible(true);

    //----------------------------------------------------------
  }
  {

    edits.put("Home", "home address");
    edits.put("School", "school address");
    edits.put("Company", "company address");

    shortCutsEDIT.put(KeyEvent.VK_S, KeyStroke.getKeyStroke("control alt S"));
    shortCutsEDIT.put(KeyEvent.VK_H, KeyStroke.getKeyStroke("control alt H"));
    shortCutsEDIT.put(KeyEvent.VK_C, KeyStroke.getKeyStroke("control alt C"));

    colors.put("Blue", Color.BLUE);
    colors.put("Yellow", Color.YELLOW);
    colors.put("Orange", Color.ORANGE);
    colors.put("Red", Color.RED);
    colors.put("White", Color.WHITE);
    colors.put("Black", Color.BLACK);
    colors.put("Green", Color.GREEN);

    fontSizes.add(8);
    fontSizes.add(10);
    fontSizes.add(12);
    fontSizes.add(14);
    fontSizes.add(16);
    fontSizes.add(18);
    fontSizes.add(20);
    fontSizes.add(22);
    fontSizes.add(24);
  }
  //Main
  public static void main(String[] args) {
      new Main();
  }
  //Create
  //------------------------------------------------------------------------BAR

  public void createMenuBar(){
    file = new JMenu("File");
    edit = new JMenu("Edit");
    options = new JMenu("Options");

    jMenuBar.add(file);
    jMenuBar.add(edit);
    jMenuBar.add(options);
  }

  public void createFile(){
    KeyStroke ctrlO = KeyStroke.getKeyStroke("control O"),
              ctrlS = KeyStroke.getKeyStroke("control S"),
              ctrlShS = KeyStroke.getKeyStroke("control shift S"),
              ctrlQ = KeyStroke.getKeyStroke("control X");

    file.add(open);
      open.setMnemonic(KeyEvent.VK_O);
      open.setAccelerator(ctrlO);
      open.setBorder(BorderFactory.createRaisedSoftBevelBorder());
    file.add(save);
      save.setMnemonic(KeyEvent.VK_S);
      save.setAccelerator(ctrlS);
      save.setBorder(BorderFactory.createRaisedSoftBevelBorder());
    file.add(saveAs);
      saveAs.setMnemonic(KeyEvent.VK_S);
      saveAs.setAccelerator(ctrlShS);
      saveAs.setBorder(BorderFactory.createRaisedSoftBevelBorder());
    file.add(line);
    file.add(exit);
      exit.setMnemonic(KeyEvent.VK_Q);
      exit.setAccelerator(ctrlQ);
      exit.setBorder(BorderFactory.createRaisedSoftBevelBorder());

    save.setEnabled(false);
    saveAs.setEnabled(false);

  }
  public void createEdit() {

    e = 0;
    for (Map.Entry<String, String> entry : edits.entrySet() ) {

        itemsEDIT.add(new JMenuItem(entry.getKey()));
        edit.add(itemsEDIT.get(e));
        itemsEDIT.get(e).setBorder(BorderFactory.createRaisedSoftBevelBorder());

        final String temp = entry.getValue();
        itemsEDIT.get(e).addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent actionEvent) {
            Object source = actionEvent.getSource();
            if (source instanceof Component) {
              area.insert(temp, area.getCaretPosition());
            }
          }
        });
        e++;
    }

    e = 0;
    for (Map.Entry<Integer, KeyStroke> entry2 : shortCutsEDIT.entrySet()) {
      itemsEDIT.get(e).setMnemonic(entry2.getKey());
      itemsEDIT.get(e).setAccelerator(entry2.getValue());
      e++;
    }



  }
  public void createOptions(){
    foreground = new JMenu("Foreground");
      foreground.setBorder(BorderFactory.createRaisedSoftBevelBorder());
    background = new JMenu("Background");
      background.setBorder(BorderFactory.createRaisedSoftBevelBorder());
    fontSize = new JMenu("Font size");
      fontSize.setBorder(BorderFactory.createRaisedSoftBevelBorder());

    options.add(foreground);
    options.add(background);
    options.add(fontSize);

    e = 0;
    for (Map.Entry<String, Color> entry : colors.entrySet())
    {
      itemsFG.add(new JMenuItem(entry.getKey(), new MyOvalIcon(entry.getValue())));
      itemsBG.add(new JMenuItem(entry.getKey(), new MyOvalIcon(entry.getValue())));
      foreground.add(itemsFG.get(e));
      background.add(itemsBG.get(e));

      itemsFG.get(e).setBorder(BorderFactory.createRaisedSoftBevelBorder());
      itemsBG.get(e).setBorder(BorderFactory.createRaisedSoftBevelBorder());

      final Color temp = entry.getValue();
      itemsBG.get(e).addActionListener( new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
          Object source = actionEvent.getSource();
          if (source instanceof Component) {
            area.setBackground(temp);
          }
        }
      });
      itemsFG.get(e).addActionListener( new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
          Object source = actionEvent.getSource();
          if (source instanceof Component) {
            area.setForeground(temp);
          }
        }
      });
      e++;
    }

    e = 0;
    for (Integer size : fontSizes){
      itemFONT.add(new JMenuItem(size+" pts"));
      fontSize.add(itemFONT.get(e));

      itemFONT.get(e).setBorder(BorderFactory.createRaisedSoftBevelBorder());

      final Font temp = new Font("Calibri", Font.BOLD, size);
      itemFONT.get(e).addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
          Object source = actionEvent.getSource();
          if (source instanceof Component) {
            area.setFont(temp);
          }
        }
      });
      e++;
    }

  }

  //-------------------------------------------------------------------------FILE

  private KeyListener k1 = new KeyAdapter() {
    public void keyPressed(KeyEvent e) {
      changed = true;
      save.setEnabled(true);
      saveAs.setEnabled(true);
    }
  };

  JMenuItem open = new JMenuItem(new AbstractAction("Open") {
    @Override
    public void actionPerformed(ActionEvent e) {
      saveOld();
      if(dialog.showOpenDialog(null)==JFileChooser.APPROVE_OPTION) {
        readInFile(dialog.getSelectedFile().getAbsolutePath());
      }
      saveAs.setEnabled(true);
    }
  });
  JMenuItem save = new JMenuItem(new AbstractAction("Save") {
    public void actionPerformed(ActionEvent e) {
      if (!currentFile.equals("Untitled"))
        saveFile(currentFile);
      else
        saveFileAs();
    }
  });
  JMenuItem saveAs = new JMenuItem(new AbstractAction("Save As ...") {
    public void actionPerformed(ActionEvent e) {
      saveFileAs();
    }
  });
  JMenuItem exit = new JMenuItem(new AbstractAction("Exit") {
    public void actionPerformed(ActionEvent e) {
      saveOld();
      System.exit(0);
    }
  });
  JPanel line = new JPanel(){
    @Override
    protected void paintComponent(Graphics graphics) {
      super.paintComponent(graphics);
      int margin = 4;
      Dimension dim = getSize();
      graphics.setColor(Color.RED);
      graphics.fillRect(5, 5, dim.width - margin *3 , dim.height - margin*3);
      this.setBorder(BorderFactory.createRaisedBevelBorder());
    }
  };

  private void saveFileAs() {
    if(dialog.showSaveDialog(null)==JFileChooser.APPROVE_OPTION)
      saveFile(dialog.getSelectedFile().getAbsolutePath());
  }
  private void saveOld() {
    if(changed) {
      if(JOptionPane.showConfirmDialog(this, "Would you like to save "+ currentFile +" ?","save",JOptionPane.YES_NO_OPTION)== JOptionPane.YES_OPTION)
        saveFile(currentFile);
    }
  }
  private void readInFile(String fileName) {
    try {
      FileReader r = new FileReader(fileName);
      area.read(r,null);
      r.close();
      currentFile = fileName;
      setTitle(currentFile);
      changed = false;
    }
    catch(IOException e) {
      Toolkit.getDefaultToolkit().beep();
      JOptionPane.showMessageDialog(this,"Can't find "+fileName);
    }
  }
  private void saveFile(String fileName) {
    try {
      FileWriter w = new FileWriter(fileName);
      area.write(w);
      w.close();
      currentFile = fileName;
      setTitle(currentFile);
      changed = false;
      save.setEnabled(false);
    }
    catch(IOException e) {
    }
  }

  //-------------------------------------------------------------------------END

}
