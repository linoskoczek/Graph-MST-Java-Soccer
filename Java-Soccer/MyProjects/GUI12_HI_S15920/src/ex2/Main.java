/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex2;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Main extends JFrame {

  Main(){
    super("Super Second Task");

    JTextField f = new JTextField("Text Field");
    final DefaultListModel<String> model = new DefaultListModel<>();
    final JList l = new JList(model);

    f.setSize(new Dimension(100, 20));
    l.setSize(new Dimension(100, 20));
    
    f.addActionListener((ActionEvent ae) -> {
      String s = f.getText();
      model.addElement(s);
    });

    //First, select an item, then press ALT and the item will be deleted
    l.addKeyListener(new KeyAdapter(){
        public void keyPressed(KeyEvent e){
            if (e.isAltDown() && model.getSize() != -1)
                try {
                    model.remove(l.getSelectedIndex());
                }catch (Exception ex){
                    System.out.println("Select an item");
                }
    }});

    JPanel p = new JPanel();

    p.add(f);
    p.add(l);

    add(p);

    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
    setSize(500, 500);
    setVisible(true);


  }

  public static void main(String[] args) {
      new Main();
  }
}
