/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex3;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.*;

public class Main extends JFrame {

  private Object[][] data;
  private JTable table;

  //Loading data from a file

  private String csvFile = "../rows.csv";
  private BufferedReader br = null;
  private String line = "";
  private String cvsSplitBy = ",";
  private int size = countLines(csvFile);
  private int rowHeight = 0;
  private String[] columnNames = {"Author", "Title", "Price", "Cover"};
  private ImageIcon img = null;


  Main() throws IOException {
    super("Super Third Task");
    data = new Object[size][];


    try {
      br = new BufferedReader(new FileReader(csvFile));
      int e = 0;
      while ((line = br.readLine()) != null) {

        // use tab as separator

        String[] row = line.split(cvsSplitBy);
        System.out.println(row[0]+"---"+row[1]+"---"+row[2]+"---"+row[3]);
        data[e] = new Object[]{row[0], row[1], row[2], new ImageIcon("../"+row[3])};
        e++;
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    img = new ImageIcon("../TLT.jpg");
    rowHeight = new ImageIcon("../TLT.jpg").getIconHeight();

    //Table stuff

    final MyTableModel model = new MyTableModel(data, columnNames);
    table = new JTable(model)
    {
      //  Returning the Class of each column will allow different
      //  Renderers to be used based on Class
      public Class getColumnClass(int column)
      {
        return getValueAt(0, column).getClass();
      }
    };
    table.setPreferredScrollableViewportSize(table.getPreferredSize());
    table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

    //Removing

    JButton remove = new JButton("Remove");
    remove.setMnemonic(KeyEvent.VK_DELETE); // Use Alt + Delete
    remove.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e){
        int selectedRow = table.getSelectedRow();
        JOptionPane.showMessageDialog(null,"The removed item : "+selectedRow);
        try {
          model.removeRow(selectedRow);
        }catch (Exception ex){}
      }
    });
    add(remove);

    //Adding

    JButton add = new JButton("Add");
    add.setMnemonic(KeyEvent.VK_INSERT); // Use Alt + Delete
    add.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e){
        try {
          String a="", t="", p="$", im="../";
          for (int i = 0; i < 4; i++){
            switch (i){
              case 0: a = JOptionPane.showInputDialog("Author");
                      break;
              case 1: t = JOptionPane.showInputDialog("Title");
                      break;
              case 2: p += JOptionPane.showInputDialog("Price in $");
                      break;
              case 3: im += JOptionPane.showInputDialog("Cover(name.extension)");
                      break;
              default: JOptionPane.showMessageDialog(null, "Error");
                      break;
            }
          }
          model.addItem(new Object[]{a, t, p, new ImageIcon(im)});
          for (int i = 0; i < model.getRowCount(); i++)
            table.setRowHeight(i, rowHeight);
        }catch (Exception ex){}
      }
    });
    add(add);

    //Editing

    JButton edit = new JButton("Edit");
    edit.setMnemonic(KeyEvent.VK_HOME); // Use Alt + Home
    edit.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e){
        try {
          model.setValueAt("O", table.getSelectedRow(), table.getSelectedColumn());
        }catch (Exception ex){}
      }
    });
    add(edit);

    //Set table cells' and columns' sizes

    for (int i = 0; i < data.length; i++)
      table.setRowHeight(i, rowHeight);

    //Add scroll pane

    JScrollPane scrollPane = new JScrollPane( table );
    getContentPane().add( scrollPane );

    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
    setSize(new Dimension(img.getIconWidth()*4, img.getIconHeight()*4));
    setLocationRelativeTo(null);
    setVisible(true);
  }

  public static void main(String[] args) throws IOException {
    new Main();

  }

  public static int countLines(String filename) throws IOException {
    InputStream is = new BufferedInputStream(new FileInputStream(filename));
    try {
      byte[] c = new byte[1024];
      int count = 0;
      int readChars = 0;
      boolean empty = true;
      while ((readChars = is.read(c)) != -1) {
        empty = false;
        for (int i = 0; i < readChars; ++i) {
          if (c[i] == '\n') {
            ++count;
          }
        }
      }
      return (count == 0 && !empty) ? 1 : count;
    } finally {
      is.close();
    }
  }

}
