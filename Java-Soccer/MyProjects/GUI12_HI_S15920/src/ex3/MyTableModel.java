package ex3;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

/**
 * Created by tiger on 13.06.17.
 */
public class MyTableModel extends AbstractTableModel {
    ArrayList<Object []> al;
    String [] header;

    public MyTableModel(Object [][] obj, String [] header){
        // save the header

        this.header = header;

        // and the rows

        al = new ArrayList<>();

        // copy the rows into the ArrayList

        for(int i = 0; i < obj.length; ++i)
        al.add(obj[i]);
    }
    public int getColumnCount() {
        return header.length;
    }
    public int getRowCount() {
        return al.size();
    }

    public Object getValueAt(int row, int col) {
        return al.get(row)[col];
    }
    public String getColumnName(int index) {
        return header[index];
    }
    void addItem(Object[] obj) {
        al.add(obj);
        fireTableDataChanged();
    }
    void removeRow(int row){
        al.remove(row);
        fireTableDataChanged();
    }

    public boolean isCellEditable(int var1, int var2) {
        return true;
    }

    public void setValueAt(Object value, int row, int col) {
        Object[] o = al.get(row);
        o[col] = value;
        al.add(row, o);
        fireTableCellUpdated(row, col);
    }
}
