package ex1;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.util.ArrayList;

/**
 * Created by tiger on 08.06.17.
 */
public class Degrees implements ListModel {

    private ArrayList<Integer> al = new ArrayList<>();

    Degrees(){
        for (int i = -70; i <= 60; i++)
            al.add(i);
    }

    @Override
    public int getSize() {
        return 60-(-70)+1;
    }

    @Override
    public Object getElementAt(int i) {
        return al.get(i)+" degrees C = "+(al.get(i)*9/5+32)+" degrees F";
    }

    @Override
    public void addListDataListener(ListDataListener listDataListener) {

    }

    @Override
    public void removeListDataListener(ListDataListener listDataListener) {

    }
}
