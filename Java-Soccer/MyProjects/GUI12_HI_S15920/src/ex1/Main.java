/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex1;


import javax.swing.*;
import java.awt.*;

public class Main extends JFrame{

  private JList list;

  public Main(){
    super("title");
    setLayout(new FlowLayout());
    Degrees degrees = new Degrees();
    list = new JList(degrees);
    JScrollPane scrollPane =
            new JScrollPane(list);
    getContentPane().add(scrollPane);

    add(scrollPane);

    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
    setLocationRelativeTo(null);
    setVisible(true);
  }

  public static void main(String[] args) {
      new Main();
  }

}
