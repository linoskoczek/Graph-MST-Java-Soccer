/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex1;


import javax.swing.*;
import java.awt.*;

public class Main {

  public static void main(String[] args) {
    JFrame frame = new JFrame("Frejm");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JTextArea textArea = new JTextArea(
            "Sam tekst hiir iz dizplejd. Ołsam, ha? Ju ken iwen skrol :DDD " +
            "Aj nid mor tekst tu czek: " +
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
            "sed do eiusmod tempor incididunt ut labore et dolore magn" +
            "a aliqua. Ut enim ad minim veniam, quis nostrud exercitat" +
            "ion ullamco laboris nisi ut aliquip ex ea commodo consequ" +
            "at. Duis aute irure dolor in reprehenderit in voluptate v" +
            "elit esse cillum dolore eu fugiat nulla pariatur. Excepte" +
            "ur sint occaecat cupidatat non proident, sunt in culpa qu" +
            "i officia deserunt mollit anim id est laborum.");
    JScrollPane scrollPane = new JScrollPane(textArea);
    textArea.setBackground(Color.blue);
    textArea.setSelectedTextColor(Color.YELLOW);
    textArea.setForeground(Color.YELLOW);
    textArea.setFont(new Font(Font.DIALOG, Font.BOLD + Font.ITALIC, 14));

    frame.add(scrollPane, BorderLayout.CENTER);
    frame.setPreferredSize(new Dimension(1000, 100));
    frame.pack();
    frame.setVisible(true);
  }
}
