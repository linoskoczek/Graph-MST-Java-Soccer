/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex3;


import javax.swing.*;
import javax.swing.border.Border;

import java.awt.*;

public class Main {

  static JLabel[] labels = new JLabel[5];
  static String[] fonts = {Font.DIALOG, Font.DIALOG_INPUT, Font.SERIF, Font.SANS_SERIF, Font.MONOSPACED};
  static String[] toolTips = {"One", "Two", "Three", "Four", "Five"};
  static String[] borderLayout = {BorderLayout.NORTH, BorderLayout.SOUTH, BorderLayout.EAST, BorderLayout.WEST, BorderLayout.CENTER};
  static Color[] colors = {Color.RED, Color.GREEN, Color.BLUE, Color.GRAY, Color.WHITE, Color.MAGENTA};
  static Border[] borders = new Border[5];
  
  public static void main(String[] args) {

    JFrame frame = new JFrame("JTextArea");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    borders[0] = BorderFactory.createDashedBorder(Color.DARK_GRAY, 5, 3);
    borders[1] = BorderFactory.createEtchedBorder(Color.GREEN, Color.BLACK);
    borders[2] = BorderFactory.createTitledBorder("Olololololololololololololololo");
    borders[3] = BorderFactory.createLineBorder(Color.MAGENTA, 20, true);
    borders[4] = BorderFactory.createMatteBorder(5, 5, 5, 5, Color.BLACK);
    
    for (int i=0;i<labels.length;i++) {
      labels[i] = new JLabel("message " + i);
      labels[i].setBackground(colors[i]);
      labels[i].setFont(new Font(fonts[i], i, 10+i));
      labels[i].setForeground(colors[labels.length-i]);
      labels[i].setToolTipText(toolTips[i]);
      labels[i].setBorder(borders[i]);
      labels[i].setOpaque(true);
    }

    frame.setLayout(new BorderLayout());
    for (int i = 0; i < labels.length; i++)
      frame.add(labels[i], borderLayout[i]);

    frame.setPreferredSize(new Dimension(1000, 500));
    frame.pack();
    frame.setVisible(true);
  }
}
