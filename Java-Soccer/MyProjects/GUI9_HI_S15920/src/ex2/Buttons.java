package ex2;

import javax.swing.*;
import java.awt.*;

/**
 * Created by tiger on 28.04.17.
 */
public class Buttons extends JPanel {

    public Buttons(boolean flag){
        if (flag) {
            setLayout(new BorderLayout());
            add(new JButton("Button 1"), BorderLayout.NORTH);
            add(new JButton("P 2"), BorderLayout.SOUTH);
            add(new JButton("The larger button number 3"), BorderLayout.EAST);
            add(new JButton("Button 4"), BorderLayout.WEST);
            add(new JButton("P5"), BorderLayout.CENTER);
        }else {
            add(new JButton("Button 1"));
            add(new JButton("P 2"));
            add(new JButton("The larger button number 3"));
            add(new JButton("Button 4"));
            add(new JButton("P5"));
        }
    }

}
