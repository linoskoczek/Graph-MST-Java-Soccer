/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex2;


import javax.swing.*;
import java.awt.*;

public class Main {

  public static void main(String[] args) {

    JFrame frame = new JFrame("Layouts");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Buttons myB = new Buttons(false);

    String name = JOptionPane.showInputDialog(frame, "Input from A to G");
    switch (name){
      case "A":
        Buttons myB2 = new Buttons(true);
        myB = myB2;
        break;
      case "B":
        myB.setLayout(new FlowLayout());
        break;
      case "C":
        myB.setLayout(new FlowLayout(FlowLayout.LEFT));
        break;
      case "D":
        myB.setLayout(new FlowLayout(FlowLayout.RIGHT));
        break;
      case "E":
        myB.setLayout(new GridLayout(1, 0));
        break;
      case "F":
        myB.setLayout(new GridLayout(0, 1));
        break;
      case "G":
        myB.setLayout(new GridLayout(3, 2));
        break;
      default:
    	  myB.setLayout(new FlowLayout());
        break;
    }

    frame.add(myB);
    frame.setPreferredSize(new Dimension(1000,100));
    frame.pack();
    frame.setVisible(true);

//    myB.setLayout(new BorderLayout());
//    b.setLayout(new FlowLayout());
//    c.setLayout(new FlowLayout(FlowLayout.LEFT));
//    d.setLayout(new FlowLayout(FlowLayout.RIGHT));
//    e.setLayout(new GridLayout(1, 0));
//    f.setLayout(new GridLayout(0, 1));
//    g.setLayout(new GridLayout(0, 1));

  }
}
