package ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Created by tiger on 21.04.17.
 */
public class ImgPanel extends JPanel {
        private static boolean isImage = true;
        private static final int TIME_DELAY = 1000;
        private BufferedImage image;


        public void putImage(BufferedImage image){
            this.image = image;
            if (image != null)
                this.setPreferredSize(new Dimension(this.image.getWidth(), this.image.getHeight()));
            else
                this.setPreferredSize(new Dimension(400, 400));
        }
        public void removeImage(){
            image = null;
            this.repaint();
            isImage = false;
        }


        protected void paintComponent(Graphics g) {
            //-------------
            super.paintComponent(g);
            Font font = g.getFont().deriveFont(20.0f);
            g.setFont(font);
            //-------------
            if (isImage && image != null) {
                    g.drawImage(image, 0, 0, getWidth(), getHeight(), null);

            } else {
                String s = "no image";
                FontMetrics fm = getFontMetrics(font);
                Rectangle2D textSize = fm.getStringBounds(s, g);
                int x = (int) ((getWidth() - textSize.getWidth()) / 2);
                int y = (int) ((getHeight() - textSize.getHeight()) / 2 + fm.getAscent());
                g.drawString(s, x, y);
            }
        }


}
