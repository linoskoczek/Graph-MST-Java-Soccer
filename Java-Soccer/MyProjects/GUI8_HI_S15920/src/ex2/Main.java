/**
 * @author Halych Illia S15920
 */

package ex2;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;


public class Main{
    private static boolean NoError = true;
    private static final int TIME_DELAY = 1000;

    //----------------------------------------------------------

    public static void main(String[] args) throws InterruptedException {
//        //----------------------------------------------------------READ_IMAGES_IN
        BufferedImage[] image = new BufferedImage[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                image[i] = ImageIO.read(new File(args[i]));
            } catch (IOException e) {
                NoError = false;
                image[i] = null;
            }
        }
//        //----------------------------------------------------------CREATE_FRAME
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        //----------------------------------------------------------//CREATE_JPANEL
        ImgPanel jp = new ImgPanel();
//        JPanel jp = new JPanel() {
//
//            @Override
//            protected void paintComponent(Graphics g) {
//                //-------------
//                super.paintComponent(g);
//                Font font = g.getFont().deriveFont(20.0f);
//                g.setFont(font);
//                //-------------
//                if (NoError) {
//
//                    for (int i = 0; i < image.length; i++)
//                        g.drawImage(image[i], 0, 0, getWidth(), getHeight(), null);
//
//                } else {
//                    String s = "no image";
//                    FontMetrics fm = getFontMetrics(font);
//                    Rectangle2D textSize = fm.getStringBounds(s, g);
//                    int x = (int) ((getWidth() - textSize.getWidth()) / 2);
//                    int y = (int) ((getHeight() - textSize.getHeight()) / 2 + fm.getAscent());
//                    g.drawString(s, x, y);
//                }
//
//
//            }
//        };
//
//        if (!NoError)
//            jp.setPreferredSize(new Dimension(450, 450));
//        else
//            jp.setPreferredSize(new Dimension(image[0].getWidth(), image[0].getHeight()));
//
//
//        //----------------------------------------------------------
//


        frame.add(jp);
        for (BufferedImage bi : image) {
            jp.putImage(bi);
            jp.repaint();
            Thread.sleep(TIME_DELAY);
        }
//        jp.removeImage();
//        jp.repaint();
//        JLabel msg = new JLabel("End of presentation");
//        jp.add(msg, BorderLayout.CENTER);

        frame.pack();
        frame.setVisible(true);

    }
}
