/**
 *
 *  @author Halych Illia S15920
 *
 */

package ex1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class Main {

  public static void main(String[] args) {
	  JFrame frame = new JFrame();
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  
	  JPanel jp = new JPanel(){
		  @Override
		  protected void paintComponent(Graphics g){
			  super.paintComponent(g);
			  g.setColor(Color.blue);
			  g.drawLine(0, 0, getWidth(), getHeight());
			  g.drawLine(0, getHeight(), getWidth(), 0);
		  }
	  };
	  jp.setPreferredSize(new Dimension(450, 450));
	  frame.add(jp);
//	  jp.repaint();
	  
	  frame.pack();
	  frame.setVisible(true);
  }
}
